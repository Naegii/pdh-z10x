@extends('adminlte::page')

@section('title', 'Admin - PdH')

@section('content_header')
    <h1>Dashboard Papo de Homem</h1>
@stop

@section('content')
    <p>Bem vindo ao Painel administrativo do Papo de Homem</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop